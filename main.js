function createNewUser() {
    
    let firstName = prompt("Enter first name:");
    let lastName = prompt("Enter last name:");
    let birthdayString = prompt("Enter your birthday(format: date.month.year):");
    let birthdaySplit = birthdayString.split(".");
    let birthday = new Date(birthdaySplit[2], birthdaySplit[1] - 1, birthdaySplit[0]);
    let todayDate = new Date();
    let username;

    let newUser = {
        firstName,
        lastName,
        birthday,
        todayDate,

        getLogin(){
            return username = firstName[0].toUpperCase() + lastName.toLowerCase();
        },

        getAge(){
            let age = Math.round((todayDate - birthday) / (1000 * 60 * 60 * 24 * 365)) ;
            return age;
        },

        getPassword(){
            let password = username + birthdaySplit[2];
            return password;
        }

        
    };
    
    return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());